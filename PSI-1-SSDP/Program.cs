﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace PSI_1_SSDP
{
    class Program
    {
        public static readonly string MULTICAST_IP_ADDRESS = "239.255.255.250";
        public static readonly int MULTICAST_PORT = 1900;
        
        public static readonly string M_SEARCH_TEXT = "M-SEARCH * HTTP/1.1";
        public static readonly string HOST_TEXT = $"HOST: {MULTICAST_IP_ADDRESS}:{MULTICAST_PORT}";
        public static readonly string MAN_TEXT = "MAN: \"ssdp:discover\"";
        public static readonly string MX_TEXT = "MX: 1";
        public static readonly string ST_TEXT = "ST: ssdp:all";


        static void Main(string[] args)
        {
            Console.WriteLine("SSDP Discovery started!\n");

            IPAddress multicastAddress = IPAddress.Parse(MULTICAST_IP_ADDRESS);
            UdpClient udpClient = new UdpClient();
            udpClient.JoinMulticastGroup(multicastAddress);

            string ssdpMessage = $"{M_SEARCH_TEXT}\r\n" +
                                 $"{HOST_TEXT}\r\n" +
                                 $"{MAN_TEXT}\r\n" +
                                 $"{MX_TEXT}\r\n" +
                                 $"{ST_TEXT}";

            Console.WriteLine("Sending following UDP message using broadcast!\n");
            Console.WriteLine(ssdpMessage);

            IPEndPoint remoteep = new IPEndPoint(multicastAddress, 1900);
            byte[] buffer = Encoding.ASCII.GetBytes(ssdpMessage);
            udpClient.Send(buffer, buffer.Length, remoteep);

            Console.WriteLine("\n\nMesage sent!\n");



            IPEndPoint groupEP = new IPEndPoint(IPAddress.Any, 0);

            try
            {
                while (true)
                {
                    Console.WriteLine("WAITING FOR RESPONSE!");
                    var bytes = udpClient.Receive(ref groupEP);
                    Console.WriteLine($"Received broadcast from {groupEP} :\n {Encoding.ASCII.GetString(bytes, 0, bytes.Length)}\n");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                //Cleanup
                udpClient.Close();
                udpClient.Dispose();
            }
        }
    }
}
