# KIV-PSI-1-SSDP

## Description of logic
- Using .NET UDPClient as Send/Recieve class, with Broadcast address and port
- Creates M_SEARCH message for SSDP
- Listens for response infinitely (It may take some time for devce to respond)

## How to use
- Execute "dotnet run" command in project folder (./PSI-1-SSDP) where .csproj is located

## Dependencies
- .NET 3.1 Runtime (or higher)
